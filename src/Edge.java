/*******************************************************
 * Edge.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * 
 *******************************************************/

public class Edge 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected int u, v;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTORS
	public Edge(int newU, int newV) 
	{ 
		u = newU;
		v = newV;
	}
	
	//////////////////////////////////////////////////////////////////
	
	// GETTERS AND SETTERS
	
	public void setU(int newU) { u = newU; }
	public void setV(int newV) { v = newV; }
	
	public int getU()	{ return u; }
	public int getV()	{ return v; }
	
	//////////////////////////////////////////////////////////////////

}
