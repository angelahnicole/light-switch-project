import java.awt.geom.Line2D;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/*******************************************************
 * Graph.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * This class performs the first two steps to solving
 * the problem.  The two primary methods in this class
 * are "readFile()" and "makeGraph()", the descriptions
 * of which are below.  The graph class, essentially, 
 * reads in the input file, and then constructs the
 * "original" graph as an adjacency matrix, or 2D array.
 * 
 *******************************************************/

public class Graph 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected File myFile = null;
	protected Vertex[] Corners;
	protected Vertex[] Lights;
	protected Vertex[] Switches;
	protected int numVerts;
	protected int[][] Graph;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public Graph(File newFile)
	{
		myFile = newFile;	
	}
	
	//////////////////////////////////////////////////////////////////
	
	// GETTER
	public int getNumVerts(){ return numVerts; }
	
	//////////////////////////////////////////////////////////////////
	
	// ==============================================================
	// METHODS THAT READ A FILE AND CREATES GRAPH FROM FILE
	// ==============================================================
	
	// --------------------------------------------------------------
	// makeGraph() METHOD
	// --------------------------------------------------------------
	// 
	// This method makes the initial, "original" graph as an
	// adjacency matrix.  The adjacency list is read ROW -> COLUMN,
	// that is, ROW 1 has a neighbor of COLUMN 2, etc.
	// This method includes a source and a sink vertex as well, so
	// the matrix includes these as well.  It automatically connects
	// the source to all the lights, and the switches to the sink.
	//
	// --------------------------------------------------------------
	public void makeGraph() 
	{
		Line2D myLine = new Line2D.Double();
		boolean intersection = false;
		Graph = new int[Lights.length + Switches.length + 2][Lights.length + Switches.length + 2];
		int LightsOffset = 1;
		int SwitchesOffset = Lights.length + 1;
		
		numVerts = Graph.length;
		
		// puts all 0's in the "original" graph
		for (int i = 0; i < Graph.length; i++) 
		{
			for (int j = 0; j < Graph.length; j++) 
			{
				Graph[i][j] = 0;
			}
		}
		
		for (int i = 0; i < Lights.length; i++) 
		{
			for (int j = 0; j < Switches.length; j++) 
			{
				for (int z = 0; z < Corners.length - 2; z++) 
				{
					myLine.setLine(Lights[i].getX(), Lights[i].getY(), Switches[j].getX(), Switches[j].getY());
					
					if (myLine.intersectsLine(Corners[z].getX(), Corners[z].getY(), Corners[z+1].getX(), Corners[z+1].getY())) 
					{
						intersection = true;
						break;
					}
				}
				
				if (!intersection)
					Graph[i + LightsOffset][j + SwitchesOffset] = 1;
				
				intersection = false;
			}
		}
		
		// the source is connected to all the lights
		for (int i = 1; i <= Lights.length; i++)
			Graph[0][i] = 1;
		
		// all the switches are connected to the sink
		for (int i = Graph.length - 2; i > Switches.length; i--)
			Graph[i][Graph.length - 1] = 1;
	}
	
	// --------------------------------------------------------------
	// readFile() METHOD
	// --------------------------------------------------------------
	//
	// This method takes in the file from the Graph constructor and
	// parses through the lines of input.  The first line is a series
	// of numbers and spaces.  This is followed by an empty  line.
	// The next lines are the coordinates of the lights.  These are
	// followed by another empty line.  The final lines are the
	// coordinates of the switches.  
	//
	// PLEASE NOTE: The file being read in MUST be in the specified
	// format, otherwise the program will not work correctly.  Errors
	// will occur.
	//
	// --------------------------------------------------------------
	public void readFile() throws Exception 
	{
		int counter = 0;
		Scanner myFileScan = new Scanner(myFile);
		String line = myFileScan.nextLine();
		Scanner myLine = new Scanner(line);
		ArrayList<Vertex> myLights = new ArrayList<Vertex>();
		ArrayList<Vertex> mySwitches = new ArrayList<Vertex>();

		// counts to see how big the corners array needs to be
		while (myLine.hasNext()) 
		{
			myLine.nextDouble();
			myLine.nextDouble();
			counter++;
		}
		
		Corners = new Vertex[counter + 1];
		counter = 0;
		myLine = new Scanner(line);
		
		// populates the corners array with the each corner's coordinates
		while (myLine.hasNext()) 
		{
			double x = myLine.nextDouble();
			double y = myLine.nextDouble();
			Vertex v = new Vertex(x, y);
			Corners[counter] = v;
			counter++;
		}
		// sets last vertex in array as the first vertex.
		// this creates a pseudo-circular array.
		Corners[Corners.length - 1] = Corners[0];
		
		// picks up the empty line
		myFileScan.nextLine();
		counter = 0;
		
		// loads up the lights array
		while (myFileScan.hasNext()) 
		{
			line = myFileScan.nextLine();  // picks up empty line when it hits it
			
			if (line.isEmpty()) break; 
			
			myLine = new Scanner(line);
			myLine.useDelimiter("[ ]+");
			
			while (myLine.hasNext())
			{
				double x = myLine.nextDouble();
				double y = myLine.nextDouble();
				myLights.add(counter, new Vertex(x, y));
			}
			
			counter++;
		}
		
		Lights = new Vertex[counter];
		
		for (int i = 0; i < Lights.length; i++) 
		{
			Lights[i] = myLights.get(i);
		}
		
		// Empty line already picked up
		counter = 0;
		
		// loads up the switches array
		while (myFileScan.hasNext()) 
		{
			line = myFileScan.nextLine();
			
			if (line.isEmpty()) break;
			
			myLine = new Scanner(line);
			myLine.useDelimiter("[ ]+");
			
			while (myLine.hasNext()) 
			{
				double x = myLine.nextDouble();
				double y = myLine.nextDouble();
				mySwitches.add(counter, new Vertex(x, y));
			}
			
			counter++;
		}
		
		Switches = new Vertex[counter];
		
		for (int i = 0; i < Switches.length; i++) 
		{
			Switches[i] = mySwitches.get(i);
		}
	}

	//////////////////////////////////////////////////////////////////
}
