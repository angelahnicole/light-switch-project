/*******************************************************
 * vertQueue.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * This is sort of a silly class- I could've just used
 * the built-in LL, but I really wanted the "dequeue" 
 * and "enqueue" methods in my queue for superficial
 * reasons.
 * 
 *******************************************************/

import java.util.LinkedList;

public class vertQueue 
{
	//////////////////////////////////////////////////////////////////
	
	protected LinkedList<Vertex> myVerts = new LinkedList<Vertex>();
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public vertQueue(){}
	
	//////////////////////////////////////////////////////////////////
	
	// Adds to end of list
	public void enqueue(Vertex v)
	{
		myVerts.add(v);
	}
	
	// Removes and returns from back of list.
	public Vertex dequeue()
	{
		return myVerts.remove();
	}
	
	// Looks without removing.
	public Vertex peek()
	{	
		return myVerts.getFirst();
	}
	
	//////////////////////////////////////////////////////////////////
	
	// Clears all elements of the queue.
	public void clear()
	{
		myVerts.clear();
	}
	
	// Returns size of queue.
	public int size()
	{
		return myVerts.size();
	}
	
	//////////////////////////////////////////////////////////////////
	
	// Returns whether or not queue is empty
	public boolean isEmpty()
	{
		return (myVerts.size() == 0);
	}
	
	//////////////////////////////////////////////////////////////////
	
}
