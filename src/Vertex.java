
/*******************************************************
 * Vertex.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * This class has two different types of constructors- 
 * double coordinates and integer coordinates. The double
 * coordinates are used to create walls and switches, and
 * the integer coordinates are used in working with the 
 * 2D array in solving the bipartite-problem.
 * 
 *******************************************************/

public class Vertex 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected double x, y;
	protected int intX, intY;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTORS
	public Vertex(double x, double y) 
	{ 
		this.x = x; this.y = y;	
	}
	public Vertex(int x, int y)
	{
		intX = x;
		intY = y;
	}
	
	//////////////////////////////////////////////////////////////////
	
	// GETTERS AND SETTERS
	
	public void setX(double x) { this.x = x; }
	public void setY(double y) { this.y = y; }
	
	public double getX() { return x; }
	public double getY() { return y; }
	public int getIntX() { return intX; }
	public int getIntY() { return intY; }
	
	//////////////////////////////////////////////////////////////////
}
