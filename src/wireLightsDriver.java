import java.io.File;
import java.util.Scanner;

/*******************************************************
 * wireLightsDriver.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * Project Specifications:
 * The electrician who is building my new house has gotten 
 * ahead of himself. The lights and light switches are in 
 * place, but are not actually wired to each other. The 
 * building code has the following constraints:
 * -Every switch controls one light, and every light has 
 *  only one switch controlling it
 * -Every switch must be in line-of-sight with the light 
 *  that it controls
 * The problem here is:
 * Given the description of a room, its lights & switches, 
 * determine how to wire the lights to switches.
 * 
 * The output is "light# switch#", separated by carriage
 * returns, with the light and switch number corresponding
 * to the order they were written on the data file.
 * 
 *******************************************************/

public class wireLightsDriver 
{
	public static void main(String[] args) 
	{
		File myFile = new File("proj3.dat");
		Graph myGraph = new Graph(myFile);
		
		try 
		{
			// Reads in the file and populates the corners, lights, and switches arrays.
			myGraph.readFile();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("Bad file.");
		}
		
		myGraph.makeGraph();

		wireLights wireIt = new wireLights(myGraph);
		
		System.out.println(wireIt.findMaxFlow());
		
	}
}