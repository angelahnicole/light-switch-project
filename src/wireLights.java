import java.util.Arrays;
import java.util.LinkedList;

/*******************************************************
 * Graph.java
 * 
 * Angela Gross
 * Andrew Meissner
 * 
 * This class solves a perfect bipartite matching problem-
 * if possible, it wires all lights to at most one switch.
 * It uses the Graph class to get the generated graph
 * and uses ford fulkerson to solve it- the comments 
 * describe fairly well how each method works together
 * to do so.
 * 
 *******************************************************/

public class wireLights 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected int [][] capacityGraph;
	protected int [][] flowGraph;
	protected int [][] residualGraph;
	protected Edge [] Pred;
	protected Edge [] Edges;
	protected boolean[] Done;
	protected LinkedList<Edge> augmentingPath;
	protected int numVerts;
	protected int numLights;
	protected Edge Sink;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public wireLights(Graph myGraph)
	{
		numVerts = myGraph.getNumVerts();
		numLights = (numVerts-2)/2;
		
		// initialize/copy graphs and augmentingPath
		capacityGraph = new int [numVerts][numVerts];
		residualGraph = new int [numVerts][numVerts];
		capacityGraph = Arrays.copyOf(myGraph.Graph, numVerts);
		residualGraph = Arrays.copyOf(myGraph.Graph, numVerts);
		
		flowGraph = new int [numVerts][numVerts];
		augmentingPath = new LinkedList<Edge>();
		
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ---------------------------------------------------------------
	// findMaxFlow() METHOD
	// ---------------------------------------------------------------
	// This method uses Ford-Fulkerson to find a bipartite matching
	// between lights and switches. As Ford-Fulkerson dictates, we
	// find an augmenting path, and if it exists, we augment the flow
	// appropriately between the residual and flow graphs. 
	// If each light is wired to a switch, then print the solution.
	// If not, then print "Impossible".
	// ---------------------------------------------------------------
	public String findMaxFlow()
	{	
		while(findAugmentingPath())
			augmentFlow();
		
		if(isAllMatched())
			return printMaxFlow();
		
		return "Impossible";	
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ==============================================================
	// METHODS THAT IMPLEMENT FORD-FULKERSON
	// ==============================================================
	
	// --------------------------------------------------------------
	// findAugmentingPath() METHOD
	// --------------------------------------------------------------
	// This is pretty standard BFS, but with a system that keeps track
	// of how it got there by keeping track of edges and predecessors.
	// An "edge" between u -> v (currVertex.x -> i) exists if the 
	// flow at the location is 1 (since we're doing bipartite matching, 
	// we're only dealing with 0s and 1s). 
	//
	// To travel through the graph, I look at each row of the adjacency
	// matrix (thus why you see lookVertex(i, 0) being added to the 
	// queue). I could've used an Edge object to represent this, but I
	// wanted to distinguish between the specific vertex I was looking
	// at between the edges I'm finding.
	//
	// When recording locations of vertices and u values of edges, 
	// I'm going with column value, or i value (so s=0, S1=1, .... 
	// t=numVerts-1). You can see that with the Edges and Predecessor 
	// arrays- This system only works because we only look at vertices
	// once due to the way that BFS deals with "doneness", and we get
	// out of BFS just as soon as we find an augmenting path.
	//
	// We find an augmenting path when we find an edge from u -> t, or
	// when the column value is numVerts-1. I trace through the path 
	// by going through the Predecessor array and add it to the 
	// augmentingPath LL. 
	// ----------------------------------------------------------------
	public boolean findAugmentingPath()
	{
		// Compliments of Angela- most possible edges/preds these
		// arrays will need.
		int optimalAlloc = numLights * numLights + numLights * 2;
		
		Done = new boolean [numVerts];
		Pred = new Edge [optimalAlloc];
		Edges = new Edge [optimalAlloc];
		
		vertQueue myQ = new vertQueue();
		Vertex Source = new Vertex(0, 0);
		Vertex currVertex, lookVertex;
		
		Edge sourceEdge = new Edge(0, 0);
		Edge currEdge, lookEdge;
		int lookFlow; 
		
		for(int i=0; i<Done.length; i++) 
			Done[i] = false;
		
		myQ.enqueue(Source);
		Done[Source.getIntY()] = true;
		Edges[0] = sourceEdge;
		
		while(!myQ.isEmpty())
		{
			currVertex = myQ.dequeue();
			currEdge = Edges[currVertex.getIntX()];

			for(int i=1; i < numVerts; i++)
			{
				lookFlow = residualGraph[currVertex.getIntX()][i];
				lookEdge = new Edge(currVertex.getIntX(), i);
				lookVertex = new Vertex(i, 0);

				if(lookFlow == 1)
				{
					if(!Done[i])
					{
						Done[i] = true;
						Edges[i] = lookEdge;
						Pred[lookEdge.getU()] = currEdge;

						if(i == numVerts-1) 
						{
							tracePath(lookEdge);
							return true;
						}
						
						myQ.enqueue(lookVertex);
					}
				}
			}
		}
		
		return false; 	
	}
	
	// ----------------------------------------------------------------
	// tracePath() METHOD
	// ----------------------------------------------------------------
	// Given an edge uv, it traces the path from uv to the source. 
	// 
	// The Predecessor array works like this:
	// 		If an edge to u -> v exists (and w -> u), then Pred[v] = wu
	// This was a little tricky to come up with, but I think it works
	// pretty well.
	// 
	// When it finds each predecessor, it adds each edge to the tail of
	// the augmentingPath LL since we start with the end of the path.
	// More specifically, we start with the edge u -> t since it's only 
	// called when there's an augmenting path.
	// ----------------------------------------------------------------
	public void tracePath(Edge uv)
	{
		do
		{
			augmentingPath.add(uv);
			uv = Pred[uv.getU()];

		}while((uv.getU() != 0));
		
		augmentingPath.add(uv); // edge from s -> u
	}
	
	// ----------------------------------------------------------------
	// augmentFlow() METHOD
	// ----------------------------------------------------------------
	// When an augmenting path is found, this method augments the flow 
	// of flowGraph and change edge orientations of residualGraph using
	// the augmentingPath LL.
	//
	// It first retrieves the coordinates of the edge found from the 
	// augmentingPath LL, and gets the capacity from the original graph.
	// It then changes edge orientations of residualGraph, a side
	// effect of the flow augmentation.
	// 
	// If the edge retrieved exists in the original graph, (i.e. if the
	// edge in capacityGraph has capacity 1), then add flow to the
	// flowGraph. If the edge doesn't exist in the original graph, then
	// take flow from the flowGraph. Since this is a bipartite matching
	// problem, flow can either be 1 or 0, so if we add flow we have a
	// flow of 1, or if we take flow we have a flow of 0.
	// ----------------------------------------------------------------
	public void augmentFlow()
	{
		Edge myEdge;
		int i, j;
		
		while(!augmentingPath.isEmpty())
		{
			myEdge = augmentingPath.pop();
			i = myEdge.getU();
			j = myEdge.getV();
			int capacity = capacityGraph[i][j];
			
			residualGraph[i][j] = 0;
			residualGraph[j][i] = 1;
			
			if(capacity == 1)
				flowGraph[i][j] = 1;
			else
				flowGraph[i][j] = 0;
		}
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ==============================================================
	// METHODS FOR FINAL OUTPUT OF MAX FLOW
	// ==============================================================
	
	// --------------------------------------------------------------
	// isAllMatched() METHOD
	// --------------------------------------------------------------
	// This method checks whether or not all lights are hooked up to
	// a single switch. In other words, checks for a maximum matching.
	// Fairly self explanatory- just goes through rows 1 through 
	// numLights (as that is where our lights are stored), and looks 
	// over the entries where a switch would lie. If a switch is hooked 
	// up to the light, then puts the switch in an array. Adds up all
	// of the entries, and if the result of the addition equals the
	// number of lights, then that means that all of the lights were
	// hooked up at most once.
	// --------------------------------------------------------------
    public boolean isAllMatched()
	{
		int [] match = new int[numLights];
		int flow = 0;
		int matches = 0;
		
		for(int i=1; i <= numLights; i++)
		{
			for(int j=numLights+1; j < numVerts; j++)
			{
				flow = flowGraph[i][j];
				if(flow == 1)
					match[i-1] = match[i-1] + flow;
			}
		}
		
		for(int i=0; i < match.length; i++)
			matches =  matches + match[i];
		
		if(matches == numLights)
			return true;
		else
			return false;
	}
	
	// --------------------------------------------------------------
	// printMaxFlow() METHOD
	// --------------------------------------------------------------
	// This method prints out the edges from light -> switch, with
	// the format being being "light# switch#", each on a new line.
	// --------------------------------------------------------------
	public String printMaxFlow()
	{
		String maxFlow = "";
		int flow = 0;
		
		for(int i=1; i <= numLights; i++)
		{
			for(int j=numLights+1; j < numVerts; j++)
			{
				flow = flowGraph[i][j];
				if(flow == 1)
					maxFlow = maxFlow + i + " " + (j-numLights) + "\n";
			}
		}
		
		return maxFlow;
	}
	
	//////////////////////////////////////////////////////////////////
}
